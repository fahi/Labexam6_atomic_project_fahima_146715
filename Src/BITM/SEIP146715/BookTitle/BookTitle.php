<?php
namespace App\BookTitle;
use App\Model\Database as DB;

class BookTitle extends DB
{
    public $id="";
    public $book_title="";
    public $author_name="";

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I am inside the index method of BookTitle class";
    }//end of construct method

    public function  setData($postVariableData= Null){
        if(array_key_exists("id",$postVariableData)){
               $this->id =$postVariableData['id'];
        }
        {
            if(array_key_exists("book_title",$postVariableData)){
                $this->book_title=$postVariableData['book_title'];
            }
        }
        {
            if(array_key_exists("author_name",$postVariableData)){
                $this->author_name =$postVariableData['author_name'];
            }
        }
    }//end of store

    public function store(){
        $sql ="insert into book_title(book_title,author_name)VALUES('$this->book_title','$this->author_name')";
        echo $sql;

        $STH= $this->DBH-> prepare($sql);
        $STH->execute();
    }


}//end of booktitle class