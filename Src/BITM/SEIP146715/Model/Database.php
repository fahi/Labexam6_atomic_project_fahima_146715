<?php
namespace App\Model;
use PDO;
use PDOException;


class Database{

    public $DBH;
    public $host = "localhost";
    public $db_name = "atomic_project_b36";
    public $user ="root";
    public $password ="";


    public function __construct(){
        try {
            $this->DBH= new PDO("mysql:$this->host;dbname:$this->db_name", $this->user, $this->password);
            echo  "connected<br>";
        }catch(PDOException $error){

            echo $error->getMessage();
        }
    }
}
